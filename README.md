Coreutils benchmarks for `Mopsa`
===============================

This repository contains scripts to analyze Coreutils programs using `Mopsa`.

Quick Start
-----------
In a terminal, type:

``` shell
$ make prepare
```
It will download Coreutils 8.30 from the GNU ftp, decompress it, and compile it.
This preliminary step is necessary to collect the source files corresponding to each program to analyze.

Then, you can analyze a single Coreutils program, for instance `true`, as:

``` shell
$ make true

```

It will perform 12 combinations of analyses corresponding to:

- 4 choices of abstract domains (A1 to A4, described later);
- 3 choices of command-line arguments (no argument, one symbolic argument, many symbolic arguments).

The first choice controls the precision / cost trade-off.
The second choice controls the execution environment, from unrealistic assumptions (no argument) to a full analysis covering all possible command-lines for the program.
The raw results are put into `results`.


Running:
``` shell
$ make stats
```
will generate the following table:

```
                                         ~ ANALYSIS ALARMS ~
+---------+---------------------------+---------------------------+---------------------------+
|         |           No arg.         |    One symbolic arg.      |    Many symbolic args.    |
| Program +---------------------------+---------------------------+---------------------------+
|         |  A1  |  A2  |  A3  |  A4  |  A1  |  A2  |  A3  |  A4  |  A1  |  A2  |  A3  |  A4  |
+---------+---------------------------+---------------------------+---------------------------+
|  true   |  0   |  0   |  0   |  0   |  89  |  61  |  61  |  13  |  89  |  61  |  61  |  13  |
+---------+---------------------------+---------------------------+---------------------------+

                                         ~ ANALYSIS TIME ~
+---------+---------------------------+---------------------------+---------------------------+
|         |           No arg.         |    One symbolic arg.      |    Many symbolic args.    |
| Program +---------------------------+---------------------------+---------------------------+
|         |  A1  |  A2  |  A3  |  A4  |  A1  |  A2  |  A3  |  A4  |  A1  |  A2  |  A3  |  A4  |
+---------+---------------------------+---------------------------+---------------------------+
|  true   | 4.57 | 5.52 | 7.78 | 7.84 | 5.26 | 6.76 | 13.1 |13.71 | 16.93| 6.7  | 12.0 | 13.22|
+---------+---------------------------+---------------------------+---------------------------+

                                           ~ COVERAGE ~
+---------+-------+---------------------------+---------------------------+---------------------------+
|         |       |           No arg.         |    One symbolic arg.      |    Many symbolic args.    |
| Program |Stmts. +---------------------------+---------------------------+---------------------------+
|         |       |  A1  |  A2  |  A3  |  A4  |  A1  |  A2  |  A3  |  A4  |  A1  |  A2  |  A3  |  A4  |
+---------+-------+---------------------------+---------------------------+---------------------------+
|  true   |  131  |  18% |  18% |  18% |  18% |  83% |  83% |  83% |  81% |  83% |  83% |  83% |  81% |
+---------+-------+---------------------------+---------------------------+---------------------------+
```

The time is in seconds.

Running:
``` shell
$ make
$ make stats

```
will run all the analyses for the all Coreutils programs.
This can take several hours, due to the number of cases to analyze.
You can speed up the analysis with `-jN`.

Supported Programs
------------------
For the moment, `Mopsa` can analyze the following 79 programs from Coreutils:

```
base32 base64 basename cat chcon chgrp chmod chown chroot cksum comm csplit cut
dircolors dirname echo env expand false fmt fold getlimits groups head hostid
id join kill link ln logname md5sum mkdir mkfifo mknod mktemp nice nl nohup nproc
numfmt od paste pathchk pinky printenv printf pwd readlink realpath rmdir runcon
seq sha1sum shred shuf sleep split stdbuf stty sync tee test timeout touch tr true
truncate tsort uname unexpand uniq unlink uptime users wc who whoami yes
```

Tab-completion after typing `make` will allow you to select a program to analyze with which scenario and configuration.

Configurations
--------------

| Configuration  | Description  |
|:--------------:|:-------------|
| A1 | `Cells o Intervals`  |
| A2 | `(Cells ∧ StringLength) o Intervals` |
| A3 | `(Cells ∧ StringLength) o (Intervals ∧ Packs(Polyhedra))`  |
| A4 | `(Cells ∧ StringLength ∧ PointerSentinel) o (Intervals ∧ Packs(Polyhedra))` |


Modification to Source Code
---------------------------
Only one change has been made on the source code of Coreutils.
The modified line is located at `src/coreutils-8.30/lib/quotearg.c:255`:

``` c
static size_t
quotearg_buffer_restyled (char *buffer, size_t buffersize,

```
which has been patched as:

``` c
size_t
quotearg_buffer_restyled (char *buffer, size_t buffersize,
```

The reason of this change is to allow the substitution of the (unsupported) recursive function `quotearg_buffer_restyled` with a stub.
Since the stub is defined in another translation unit, we need to remove the `static` keyword so that `quotearg_buffer_restyled` is uniquely resolved in all translation units.
The stub replacing the function can be found in `support/stubs.c`.
