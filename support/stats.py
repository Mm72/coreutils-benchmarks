#!/usr/bin/python3
##############################################################################
#                                                                            #
#  This file is part of MOPSA, a Modular Open Platform for Static Analysis.  #
#                                                                            #
#  Copyright (C) 2017-2019 The MOPSA Project.                                #
#                                                                            #
#  This program is free software: you can redistribute it and/or modify      #
#  it under the terms of the GNU Lesser General Public License as published  #
#  by the Free Software Foundation, either version 3 of the License, or      #
#  (at your option) any later version.                                       #
#                                                                            #
#  This program is distributed in the hope that it will be useful,           #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of            #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             #
#  GNU Lesser General Public License for more details.                       #
#                                                                            #
#  You should have received a copy of the GNU Lesser General Public License  #
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.     #
#                                                                            #
##############################################################################

# Script to print statistics on coreutils benchmarks

import argparse
import os
import json
import re
from common import *



# Get the command line arguments
def get_args():
    parser = argparse.ArgumentParser(description='Script to print statistics on coreutils benchmarks.')
    parser.add_argument('results',
                        help="Directory where results are places")
    return parser.parse_args()

class Report:
    def __init__(self, json_path, log_path):
        try:
            # Get alarms and time
            with open(json_path, 'r') as f:
                data = json.load(f)
                self.time = data['time']
                self.success = data['success']
                if self.success:
                    self.alarms = len([c for c in data['checks'] if c['kind'] == 'warning'])
                    self.assumptions = len(data['assumptions'])
                else:
                    self.alarms = None
                    self.assumptions = None
            # Get coverage
            with open(log_path, 'r') as f:
                # Logs are plain text without structure
                # So we assume that logs of the coverage hook are placed first
                # The second line corresponds to the global coverage
                _ = f.readline()
                line = f.readline().strip()
                parts = line.split(' ')
                self.covered_stmt = int(parts[0][:-1])*int(parts[2])
                self.total_stmt = int(parts[2])
        except:
            self.time = None
            self.success = None
            self.alarms = None
            self.assumptions = None
            self.covered_stmt = None
            self.total_stmt = 0


class AbstractionReport:
    def __init__(self, progam, scenario, abstraction, results):
        self.progam = progam
        self.scenario = scenario
        self.abstraction = abstraction['name']
        json_path = os.path.join(results, "%s-%s-%s.json"%(program, scenario['name'], abstraction['name']))
        log_path = os.path.join(results, "%s-%s-%s.log"%(program, scenario['name'], abstraction['name']))
        self.report = Report(json_path, log_path)
        self.max_total_stmt = self.report.total_stmt



class ScenarioReport:
    def __init__(self, progam, scenario, results):
        self.reports = []
        self.max_total_stmt = 0
        self.progam = progam
        self.scenario = scenario['name']
        for abstraction in ABSTRACTIONS.values():
            r = AbstractionReport(progam, scenario, abstraction, results)
            self.reports.append(r)
            self.max_total_stmt = max(self.max_total_stmt, r.max_total_stmt)


class ProgramReport:
    def __init__(self, progam, results):
        self.reports = []
        self.max_total_stmt = 0
        self.program = program
        for scenario in SCENARIOS.values():
            r = ScenarioReport(progam, scenario, results)
            self.reports.append(r)
            self.max_total_stmt = max(self.max_total_stmt, r.max_total_stmt)


# Get the list of progams in the results directory
def get_programs(results):
    programs = set()
    for o in os.listdir(results):
        m = re.search('([^-]+)-.*', o)
        if m is not None:
            programs.add(m.group(1))
    return sorted(list(programs))



# Collect reports
args = get_args()
reports = []
for program in get_programs(args.results):
    reports.append(ProgramReport(program, args.results))

# Alarms
print("{:^143}".format('~ ANALYSIS ALARMS ~'))
print("+-{:-<9}-+-{:-<41}-+-{:-<41}-+-{:-<41}-+".format('','','',''))
print("| {:^9} | {:^41} | {:^41} | {:^41} |".format('','No arg.','One symbolic arg.','Many symbolic args.'))
print("| {:^9} +-{:-<41}-+-{:-<41}-+-{:-<41}-+".format('Program','','',''))
print("| {:^9} | {:^8} | {:^8} | {:^8} | {:^8} | {:^8} | {:^8} | {:^8} | {:^8} | {:^8} | {:^8} | {:^8} | {:^8} |".format('','A1','A2','A3','A4','A1','A2','A3','A4','A1','A2','A3','A4'))
print("+-{:-<9}-+-{:-<41}-+-{:-<41}-+-{:-<41}-+".format('','','',''))
for r in reports:
    line = {}
    for rr in r.reports:
        for rrr in rr.reports:
            line["%s-%s"%(rr.scenario,rrr.abstraction)] = rrr.report.alarms if rrr.report.alarms is not None else '-'
    print("| {:^9} | {line[no-arg-a1]:^8} | {line[no-arg-a2]:^8} | {line[no-arg-a3]:^8} | {line[no-arg-a4]:^8} | {line[one-symbolic-arg-a1]:^8} | {line[one-symbolic-arg-a2]:^8} | {line[one-symbolic-arg-a3]:^8} | {line[one-symbolic-arg-a4]:^8} | {line[many-symbolic-args-a1]:^8} | {line[many-symbolic-args-a2]:^8} | {line[many-symbolic-args-a3]:^8} | {line[many-symbolic-args-a4]:^8} |".format(r.program,line=line))
print("+-{:-<9}-+-{:-<41}-+-{:-<41}-+-{:-<41}-+".format('','','',''))



# Time
print()
print("{:^143}".format('~ ANALYSIS TIME ~'))
print("+-{:-<9}-+-{:-<41}-+-{:-<41}-+-{:-<41}-+".format('','','',''))
print("| {:^9} | {:^41} | {:^41} | {:^41} |".format('','No arg.','One symbolic arg.','Many symbolic args.'))
print("| {:^9} +-{:-<41}-+-{:-<41}-+-{:-<41}-+".format('Program','','',''))
print("| {:^9} | {:^8} | {:^8} | {:^8} | {:^8} | {:^8} | {:^8} | {:^8} | {:^8} | {:^8} | {:^8} | {:^8} | {:^8} |".format('','A1','A2','A3','A4','A1','A2','A3','A4','A1','A2','A3','A4'))
print("+-{:-<9}-+-{:-<41}-+-{:-<41}-+-{:-<41}-+".format('','','',''))
for r in reports:
    line = {}
    for rr in r.reports:
        for rrr in rr.reports:
            line["%s-%s"%(rr.scenario,rrr.abstraction)] = round(rrr.report.time,2) if rrr.report.time is not None else '-'
    print("| {:^9} | {line[no-arg-a1]:^8} | {line[no-arg-a2]:^8} | {line[no-arg-a3]:^8} | {line[no-arg-a4]:^8} | {line[one-symbolic-arg-a1]:^8} | {line[one-symbolic-arg-a2]:^8} | {line[one-symbolic-arg-a3]:^8} | {line[one-symbolic-arg-a4]:^8} | {line[many-symbolic-args-a1]:^8} | {line[many-symbolic-args-a2]:^8} | {line[many-symbolic-args-a3]:^8} | {line[many-symbolic-args-a4]:^8} |".format(r.program,line=line))
print("+-{:-<9}-+-{:-<41}-+-{:-<41}-+-{:-<41}-+".format('','','',''))

# Coverage
# Time
print()
print("{:^143}".format('~ COVERAGE ~'))
print("+-{:-<9}-+-{:-<9}-+-{:-<37}-+-{:-<37}-+-{:-<37}-+".format('','','','',''))
print("| {:^9} | {:^9} | {:^37} | {:^37} | {:^37} |".format('','','No arg.','One symbolic arg.','Many symbolic args.'))
print("| {:^9} | {:^9} +-{:-<37}-+-{:-<37}-+-{:-<37}-+".format('Program','Stmts.','','',''))
print("| {:^9} | {:^9} | {:^7} | {:^7} | {:^7} | {:^7} | {:^7} | {:^7} | {:^7} | {:^7} | {:^7} | {:^7} | {:^7} | {:^7} |".format('','','A1','A2','A3','A4','A1','A2','A3','A4','A1','A2','A3','A4'))
print("+-{:-<9}-+-{:-<9}-+-{:-<37}-+-{:-<37}-+-{:-<37}-+".format('','','','',''))
for r in reports:
    line = {}
    for rr in r.reports:
        for rrr in rr.reports:
            line["%s-%s"%(rr.scenario,rrr.abstraction)] = '{:.0f}%'.format(float(rrr.report.covered_stmt)/r.max_total_stmt) if rrr.report.covered_stmt is not None else '-'
    print("| {:^9} | {:^9} | {line[no-arg-a1]:^7} | {line[no-arg-a2]:^7} | {line[no-arg-a3]:^7} | {line[no-arg-a4]:^7} | {line[one-symbolic-arg-a1]:^7} | {line[one-symbolic-arg-a2]:^7} | {line[one-symbolic-arg-a3]:^7} | {line[one-symbolic-arg-a4]:^7} | {line[many-symbolic-args-a1]:^7} | {line[many-symbolic-args-a2]:^7} | {line[many-symbolic-args-a3]:^7} | {line[many-symbolic-args-a4]:^7} |".format(r.program,r.max_total_stmt,line=line))
print("+-{:-<9}-+-{:-<9}-+-{:-<37}-+-{:-<37}-+-{:-<37}-+".format('','','','',''))
